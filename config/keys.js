var NodeRSA = require('node-rsa');
var key = new NodeRSA({
	b: 1024
});

key.setOptions({
	encryptionScheme: 'pkcs1'
});

var publicKey = key.exportKey('public');
var privateKey = key.exportKey('private');

module.exports = {
	getPublicKey: function () {
		return publicKey;
	},
	getPrivateKey: function () {
		return privateKey;
	},
	decrypt: function (buffer) {
		return key.decrypt(buffer, 'utf8');
	}
};
