// Router initialisation
var express = require('express');
var router = express.Router();

// Load module dependencies
var mongoose = require('mongoose');
var TestDraft = mongoose.model("TestDraft");
var jwt = require('express-jwt');


// Create the JWT for the routes
var auth = jwt({
    secret: 'SECRET',
    userProperty: 'payload'
}); // SECRET is to be changed later on


/* ----- BEGIN ROUTES ----- */
// - Store latest/updated localSavedTest
router.post('/createTestDraft', function(req, res, next) {
    var iden = req.body.identity;
    var code = req.body.codec;
    var data = {
        identity: iden,
        code: code
    };
    console.log(iden);
    var testDraft = new TestDraft(data);
    TestDraft.findOne({
        code: code
    }, function(err, TestDraft) {
        if (err) return handleError(err);
        //console.log(TestDraft);
        //create if table does not exist
        if (TestDraft == null) {
            testDraft.save(function(err, TestDraft) {
                if (err) {
                    return next(err);
                }
                res.json(TestDraft);
            });
        }
        res.json(TestDraft);
})
});

router.post('/updateTestDraft', function(req, res, next) {
    //var oldID = req.body.iden;
    var newID = req.body.newIden;
    var codec = req.body.constant;
    TestDraft.update({
        code: codec
    }, {
        $set: {
            identity: newID
        }
    }, function(err, updatedTestDraft) {
        if (err) {
            return next(err);
        }
    if(updatedTestDraft.nModified == 1){
        TestDraft.find({
            code: codec
        },
        function(err, displayTestDraft) {
            if (err) {
                return next(err);
            }

            res.json(displayTestDraft);
        });
    }else{
        res.json(updatedTestDraft);
    }
    })
});

router.post('/getTestDraft', function(req, res, next) {
    var codec = req.body.codec;
    TestDraft.find({
        code: codec
    }, function(err, testDraft) {
        if (err) {
            return next(err);
        }

        res.json(testDraft);


    });
});


module.exports = router;
