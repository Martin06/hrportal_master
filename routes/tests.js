// Router initialisation
var express = require('express');
var router = express.Router();

// Load module dependencies
var mongoose = require('mongoose');
var Test = mongoose.model('Test');
var Objective = mongoose.model('Objective');
var Subjective = mongoose.model('Subjective');
var jwt = require('express-jwt');

// Create the JWT for the routes
var auth = jwt({
    secret: 'SECRET',
    userProperty: 'payload'
}); // SECRET is to be changed later on

/* ----- BEGIN ROUTES ----- */
// - Get the list of all tests
router.get('/get', function(req, res, next) {
    Test.find(function(err, tests) {
        if (err) {
            return next(err);
        }
        //console.log(tests);
        res.json(tests);
    });
});

// - Create a new test
router.post('/insert', auth, function(req, res, next) {
    console.log(req.body);
    var test = new Test(req.body);

    test.save(function(err, tests) {
        if (err) {
            return next(err);
            console.log(err);
        }

        res.json(tests);
    });
});


//delete a specific test by id
router.post('/delete', auth, function(req, res, next) {

    Test.remove({
        _id: req.body.id
    }, function(err) {
        if (err) {
            return next(err);
        }

        // Requery the new list after deletion
        Test.find(function(err, tests) {
            if (err) {
                return next(err);
            }

            res.json(tests);
        });
    });
});


//get a specific test for update
router.post('/getone', function(req, res, next) {
    var id = req.body.id;

    Test.findOne({
        _id: id
    }, function(err, tests) {
        if (err) {
            return next(err);
        }

        res.json(tests);
        console.log(tests);

    });
});

//update a specific test by id
router.post('/update', auth, function(req, res, next) {

    Test.findByIdAndUpdate(req.body.id, {
        $set: {
            name: req.body.name,
            level: req.body.level,
            category: req.body.category,
            totaltest: req.body.totaltest,
            passingmark: req.body.passingmark,
            section: req.body.section,
            question_type: req.body.question_type
        }

    }, function(err, tests) {
        if (err) {
            return next(err);
        }

        res.json(tests);

    })
});


router.post('/getobjquestion', auth, function(req, res, next) {
    var type = req.body.question_type.objtype;
    var level = req.body.level;
    var category = req.body.category;


    Objective.find({
        answertype: type,
        level: level,
        category: {
            $elemMatch: {
                text: category,
            }
        }
    }, function(err, tests) {
        if (err) {
            return next(err);
        }

        res.json(tests);
    })
});

router.post('/getsubquestion', auth, function(req, res, next) {


    var level = req.body.level;
    var category = req.body.category;


    Subjective.find({
        level: level,
        category: {
            $elemMatch: {
                text: category,
            }
        }
    }, function(err, tests) {
        if (err) {
            return next(err);
        }

        res.json(tests);
    })
});

router.post('/getcandidatetest', auth, function(req, res, next) {

    Test.find({
        level: req.body.testslevel,
        category: req.body.teststest
    }, function(err, tests) {
        if (err) {
            return next(err);
        }

        res.json(tests);
    })
});

router.get('/getcategory', auth, function(req, res, next) {



    Objective.find({

    }, function(err, tests) {
        if (err) {
            return next(err);
        }

        res.json(tests);
    })
});

router.post('/getLevelAndCategory', function(req, res, next) {

    Test.find({
        level: req.body.level,
        category: req.body.testassign
    }, function(err, tests) {
        if (err) {
            return next(err);
        }

        res.json(tests);
    })
});

router.post('/getpassingmark', function(req, res, next){

    Test.find({
        level: req.body.level,
        category: req.body.category
        
    }, function(err, tests) {
        if (err) {
            return next(err);
        }

        res.json(tests);
    });
});

module.exports = router;
