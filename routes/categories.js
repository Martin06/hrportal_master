// Router initialisation
var express = require('express');
var router = express.Router();

// Load module dependencies
var mongoose = require('mongoose');
var Category = mongoose.model('Category');
var jwt = require('express-jwt');

// Create the JWT for the routes
var auth = jwt({
    secret: 'SECRET',
    userProperty: 'payload'
}); // SECRET is to be changed later on

/* ----- BEGIN ROUTES ----- */
// - Get the list of all categories
router.get('/get', function (req, res, next) {
    Category.find(function (err, categories) {
        if (err) {
            return next(err);
        }

        res.json(categories);
    });
});

// - Create a new category
router.post('/insert', auth, function (req, res, next) {
    var category = new Category(req.body);

    category.save(function (err, category) {
        if (err) {
            return next(err);
        }
        res.json(category);
    });
});

module.exports = router;

