// Router initialisation
var express = require('express');
var router = express.Router();

// Load module dependencies
var mongoose = require('mongoose');
var Candidate = mongoose.model('Candidate');
var User = mongoose.model('User');
var Test = mongoose.model("Test");
var jwt = require('express-jwt');

// Create the JWT for the routes
var auth = jwt({
    secret: 'SECRET',
    userProperty: 'payload'
}); // SECRET is to be changed later on

/* ----- BEGIN ROUTES ----- */
// - Get the list of all candidates
router.get('/get', function(req, res, next) {
    Candidate.find(function(err, candidates) {
        if (err) {
            return next(err);
        }

        res.json(candidates);
    });
});

// - Create a new candidate
router.post('/insert', auth, function(req, res, next) {
    var candidate = new Candidate(req.body);

    candidate.save(function(err, candidate) {
        if (err) {
            return next(err);
        }

        res.json(candidate);
    });
});

// - Update a candidate
router.post('/update', auth, function(req, res, next) {

    Candidate.findByIdAndUpdate(req.body._id, {
        $set: {
            name: req.body.name,
            from: req.body.from,
            background: req.body.background,
            phone: req.body.phone,
            startdate: req.body.startdate,
            enddate: req.body.enddate,
            email: req.body.email,
            results: req.body.results,
        }
    }, function(err, candidate) {
        if (err) {
            return next(err);
        }

        res.json(candidate);
    })
});


router.post('/updateuser', auth, function(req, res, next) {
    var oldname = req.body.oldName;
    var newname = req.body.candidate.name;
    User.update({
        username: oldname,

    }, {
        $set: {
            username: newname,
        }
    }, function(err, candidate) {
        if (err) {
            return next(err);
        }

    })

});




// - Delete a candidate
router.post('/delete', auth, function(req, res, next) {
    Candidate.remove({
        _id: req.body.id
    }, function(err) {
        if (err) {
            return next(err);
        }

        // Requery the new list after deletion
        Candidate.find(function(err, candidates) {
            if (err) {
                return next(err);
            }

            res.json(candidates);
        });
    });
});

router.post('/deleteuser', auth, function(req, res, next) {
    var name = req.body.username;
    User.remove({
        username: name
    }, function(err, candidates) {
        if (err) {
            return next(err);
        }
    });

});



router.post('/getone', function(req, res, next) {

    var user = req.body.user1;
    Candidate.find({
            name: user
        },
        function(err, candidates) {
            if (err) {
                return next(err);
            }

            res.json(candidates);
        });
});

router.post('/gettest', function(req, res, next) {

    Test.find({
            level: req.body.candidatetestslevel,
            category: req.body.candidateteststest
        },
        function(err, candidates) {
            if (err) {
                return next(err);
            }

            res.json(candidates);
        });
});

router.post('/testdetail', function(req, res, next) {
    var id = req.body.id;

    Test.find({
        _id: id
    }, function(err, candidates) {
        if (err) {
            return next(err);
        }

        res.json(candidates);


    });
});



router.post('/savetest', function(req, res, next) {
    var user = req.body.user;
    var x = req.body.update;

    Candidate.update({
        name: user
    }, {
        $set: {
            name: x.name,
            from: x.from,
            background: x.background,
            gender: x.gender,
            phone: x.phone,
            startdate: x.startdate,
            enddate: x.enddate,
            selected: x.selected,
            email: x.email,
            status: x.status,
            password: x.password,
            tests: x.tests,
            markstatus: x.markstatus,
            results: x.results
                /*"tests.$.objmark": mark,
                "tests.$.subans": answer,
                status: "Yes",
                "tests.$.status": "Yes",
                "tests.$.attempted": true*/
        }
    }, function(err, candidate) {
        if (err) {
            return next(err);
        }
        res.json(candidate);
    })
})

router.post('/findTestCandidate', function(req, res, next) {
    var user = req.body.user;

    Candidate.find({
            name: user
        },
        function(err, candidate) {
            if (err) {
                return next(err);
            }
            //console.log(candidate[0].tests);
            res.json(candidate);
        });
});

router.post('/attemptedTest', function(req, res, next) {
    var user = req.body.user;
    //var id = req.body.id;

    Candidate.find({
            name: user
                /*,
                            $or: [{
                                    test1: req.body.id
                                }, {
                                    test2: req.body.id
                                }
                            ]*/
        },
        function(err, candidate) {
            if (err) {
                return next(err);
            }
            res.json(candidate);
        });
});

router.post('/updatemark', function(req, res, next) {

    Candidate.update({
        name: req.body.candidatename,
        "tests.test": req.body.candidatetest,
    }, {
        $set: {
            "tests.$.submark": req.body.subMark,
            "tests.$.totalmark": req.body.totalmark,
            markstatus: 0

        }
    }, function(err, candidate) {
        if (err) {
            return next(err);
        }

        res.json(candidate);
    })
});

router.post('/updateresults', function(req, res, next) {

    Candidate.update({
        name: req.body.candidatename,
    }, {
        $set: {
            results: req.body.results
        }
    }, function(err, candidate) {
        if (err) {
            return next(err);
        }

        res.json(candidate);
    })
});

module.exports = router;
