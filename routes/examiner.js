// Router initialisation
var express = require('express');
var router = express.Router();

// Load module dependencies
var mongoose = require('mongoose');
var Examiner = mongoose.model('Examiner');
var User = mongoose.model('User');
var jwt = require('express-jwt');

// Create the JWT for the routes
var auth = jwt({
    secret: 'SECRET',
    userProperty: 'payload'
}); // SECRET is to be changed later on

/* ----- BEGIN ROUTES ----- */
// - Get the list of all examiners
router.get('/get', function (req, res, next) {
    Examiner.find(function (err, examiners) {
        if (err) {
            return next(err);
        }

        res.json(examiners);
    });
});

// - Create a new examiner
router.post('/insert', auth, function (req, res, next) {
    var examiner = new Examiner(req.body);

    examiner.save(function (err, examiner) {
        if (err) {
            return next(err);
        }

        res.json(examiner);
    });
});

// - Delete an examiner
router.post('/delete', auth, function (req, res, next) {

    Examiner.remove({
        _id: req.body.id
    }, function (err) {
        if (err) {
            return next(err);
        }

        // Requery the new list after deletion
        Examiner.find(function (err, examiners) {
            if (err) {
                return next(err);
            }

            res.json(examiners);
        });
    });
});

router.post('/deleteuser', auth, function(req, res, next) {
     console.log(req.body.email);
     User.remove({
        username: req.body.email
    }, function(err, examiner) {
        if (err) {
            return next(err);
        }
    });

});

// - Update an examiner
router.post('/update', auth, function (req, res, next) {


    Examiner.findByIdAndUpdate(req.body._id, {
        $set: {
            name: req.body.name,
            department: req.body.department,
            position:req.body.position,
            email: req.body.email,
            password: req.body.password
        }
    }, function (err, examiner) {
        if (err) {
            return next(err);
        }

        res.json(examiner);
    })
});


router.post('/updateemail', auth, function(req, res, next) {
   
    var email = req.body.oldEmail;
    var newemail = req.body.examiner.email;
    User.update({
        username: email,

    }, {
        $set: {
            username: newemail,
        }
    }, function(err, candidate) {
        if (err) {
            return next(err);
        }

    })

});


module.exports = router;
