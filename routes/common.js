// Router initialisation
var express = require('express');
var router = express.Router();

// Load module dependencies
var mongoose = require('mongoose');
var passport = require('passport');
var User = mongoose.model('User');

// Creation of public and private keys
var keys = require('./../config/keys');
var publicKey = keys.getPublicKey();

/* ----- BEGIN ROUTES ----- */
// - Sends the public key to the front-end
router.get('/key', function (req, res, next) {
    res.json({
        "publickey": publicKey
    });
});

// - Registers a new user
router.post('/register', function (req, res, next) {
       if (!req.body.username || !req.body.password || !req.body.role) {
        return res.status(400).json({
            message: 'Please fill out all fields'
        });
    }

    var user = new User();
    user.username = req.body.username;
    user.role = req.body.role;
    user.setPassword(req.body.password);

    user.save(function (err) {
        if (err) {
            return next(err);
        }

        return res.json({
            token: user.generateJWT()
        });
    });
});

// - Login checking
router.post('/login', function (req, res, next) {
    if (!req.body.credentials) {
        return res.status(400).json({
            message: 'Please fill out all fields'
        });
    }

    var decryptedString = keys.decrypt(req.body.credentials);

    if (decryptedString.indexOf(':') < 0 && (!req.body.username || !req.body.password)) {
        return res.status(400).json({
            message: 'Please fill out all fields'
        });
    }

    var splitDecryptedString = decryptedString.split(':');

    // Passport takes the req.body.username and req.body.password and passes it to our verification function in the local strategy.
    req.body.username = splitDecryptedString[0];
    req.body.password = splitDecryptedString[1];

    passport.authenticate('local', function (err, user, info) {
        if (err) {
            return next(err);
        }

        if (user) {
            return res.json({
                token: user.generateJWT()
            });
        } else {
            return res.status(401).json(info);
        }
    })(req, res, next);
});

module.exports = router;