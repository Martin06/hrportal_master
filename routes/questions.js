// Router initialisation
var express = require('express');
var router = express.Router();

// Load module dependencies
var mongoose = require('mongoose');
var Objective = mongoose.model('Objective');
var Subjective = mongoose.model('Subjective');
var Category = mongoose.model('Category');

var jwt = require('express-jwt');

// Create the JWT for the routes
var auth = jwt({
	secret: 'SECRET',
	userProperty: 'payload'
}); // SECRET is to be changed later on

/* ----- BEGIN ROUTES ----- */
// - SUBJECTIVE QUESTIONS
// -- Get all subjective questions
router.get('/subjective/get', function (req, res, next) {
	Subjective.find(function (err, subjectiveQuestions) {
		if (err) {
			return next(err);
		}

		res.json(subjectiveQuestions);
	});
});

// -- Get a specific subjective question
router.get('/subjective/get/:id', function (req, res, next) {
	Subjective.findOne({
		_id: req.params.id
	}, function (err, subjectiveQuestion) {
		if (err) {
			return next(err);
		}

		res.json(subjectiveQuestion);
	});
});

//-- Insert a new subjective question
router.post('/subjective/insert', auth, function (req, res, next) {
	var subjectiveQuestion = new Subjective(req.body);

	subjectiveQuestion.save(function (err, subjectiveQuestion) {
		if (err) {
			return next(err);
		}

		res.json(subjectiveQuestion);
	});
});

// -- Delete a subjective question
router.post('/subjective/delete', auth, function (req, res, next) {
	Subjective.remove({
		_id: req.body.id
	}, function (err) {
		if (err) {
			return next(err);
		}

		// Requery the new list after deletion
		Subjective.find(function (err, subjectiveQuestions) {
			if (err) {
				return next(err);
			}

			res.json(subjectiveQuestions);
		});
	});
});

// -- Update a subjective question
router.post('/subjective/update', auth, function (req, res, next) {
    Subjective.findByIdAndUpdate(req.body.id, {
        $set: {
            question: req.body.question,
            category: req.body.category,
            level: req.body.level,
            answer_scheme: req.body.answer_scheme,
            mark: req.body.mark

        }
    }, function (err, subjectiveQuestion) {
        if (err) {
            return next(err);
        }

        res.json(subjectiveQuestion);
    })
});

// - OBJECTIVE QUESTIONS
// -- Get all objective questions
router.get('/objective/get', function (req, res, next) {
	Objective.find(function (err, objectiveQuestions) {
		if (err) {
			return next(err);
		}

		res.json(objectiveQuestions);
	});
});

// -- Get a specific objective question
router.get('/objective/get/:id', function (req, res, next) {
	Objective.findOne({
		_id: req.params.id
	}, function (err, objectiveQuestion) {
		if (err) {
			return next(err);
		}

		res.json(objectiveQuestion);
	});
});

// -- Insert a new objective question
router.post('/objective/insert', auth, function (req, res, next) {
	var objectiveQuestion = new Objective(req.body);

	objectiveQuestion.save(function (err, objectiveQuestion) {
		if (err) {
			return next(err);
		}

		res.json(objectiveQuestion);
	});
});

// -- Delete an objective question
router.post('/objective/delete', auth, function (req, res, next) {
	Objective.remove({
		_id: req.body.id
	}, function (err) {
		if (err) {
			return next(err);
		}

		// Requery the new list after deletion
		Objective.find(function (err, objectiveQuestions) {
			if (err) {
				return next(err);
			}

			res.json(objectiveQuestions);
		});
	});
});

// -- Update an objective question
router.post('/objective/update', auth, function (req, res, next) {
    Objective.findByIdAndUpdate(req.body.id, {
        $set: {
        	question: req.body.question,
            category: req.body.category,
            level: req.body.level,
	        answers: {
	             answer: req.body.answers.answer,
	             correct_answer:  req.body.answers.correct_answer
	         },
            mark: req.body.mark

        }
    }, function (err, objectiveQuestion) {
        if (err) {
            return next(err);
        }

        res.json(objectiveQuestion);
    })
});
module.exports = router;