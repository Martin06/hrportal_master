module.exports = function (grunt) {

	// Project configuration.
	grunt.initConfig({
		uglify: {
			all_src: {
				options: {
					sourceMap: true,
					sourceMapName: 'public/app/app.min.map'
				},
				src: 'public/app/**/*.js',
				dest: 'public/app/app.min.js'
			}
		}
	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-uglify');

	// Default task(s).
	grunt.registerTask('default', ['uglify']);
};