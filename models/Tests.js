var mongoose = require('mongoose');

var TestSchema = new mongoose.Schema({
	name: String,
	level: String,
	category: String,
	totaltest: Number,
	passingmark: Number,
	duplicate: Boolean,
	section: [{
		question_obj: [{
			level: String,
			mark: Number,
			question: String,
			answertype: String,
			category:[{
				text: String,
			}],
			answers: {
		answer: [],
		correct_answer: []
	}
		
	}],
	question_sub: [{
			level: String,
			mark: Number,
			question: String,
			answer_scheme: String,
			category:[{
				text: String,
			}]	
		
	}],
		name: String,
		time: Number,
		instruction: String,
		totalmark: Number,
		// question: Number,
		// obj: String,
		// objtype: String
	}],
	question_type:[{
		obj: String,
		objtype: String,
	}],
	
});

mongoose.model('Test', TestSchema);