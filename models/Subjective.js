var mongoose = require('mongoose');

var SubjectiveSchema = new mongoose.Schema({
	question: String,
	answer_scheme: String,
	category: [{
		text:String
	}],
	mark: Number,
	level: String
});

mongoose.model('Subjective', SubjectiveSchema);