var mongoose = require('mongoose');

var ObjectiveSchema = new mongoose.Schema({
	question: String,
	answertype: String,
	answers: {
		answer: [],
		correct_answer: []
	},
	category: [{
		text: String
	}],
	mark: Number,
	level: String
});

mongoose.model('Objective', ObjectiveSchema);