var mongoose = require('mongoose');

var CandidateSchema = new mongoose.Schema({
    name: String,
    from: String,
    background: String,
    gender: String,
    phone: String,
    startdate: Date,
    enddate: Date,
    selected: Boolean,
    email: String,
    status: String,
    password: String,
    tests : [{
        test: String,
        level: String,
        objmark: Number,
        subans: [],
        submark: Number,
        totalmark: Number,
        outOff: Number,
        status: String,
        attempted: Boolean
    }],
    markstatus: Number,
    results: String
});

mongoose.model('Candidate', CandidateSchema);
