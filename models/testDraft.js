var mongoose = require('mongoose');

var TestDraftSchema = new mongoose.Schema({
	code: {type: Number, default: 1},
	identity: mongoose.Schema.ObjectId
});

mongoose.model('TestDraft', TestDraftSchema);