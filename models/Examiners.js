var mongoose = require('mongoose');

var ExaminerSchema = new mongoose.Schema({
    name: String,
    department: String,
    position:String,
    email: String,
    password: String
});

mongoose.model('Examiner', ExaminerSchema);
