var mongoose = require('mongoose');

var CandidateListSchema = new mongoose.Schema({
	question: String,	
	answers: {
		answer: [],
		correct_answer: []
	},
	category:String,
	candidatename:String,
	candidateanswer:String,
	mark: Number,
	level: String
});

mongoose.model('CandidateList', CandidateListSchema);