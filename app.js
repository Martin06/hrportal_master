var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

/* The Mongoose and Passport libraries are loaded here,        **
 **  as well as the corresponding models from the models folder */
// ------------- Start -------------
var mongoose = require('mongoose');
var passport = require('passport');
require('./models/Candidates');
require('./models/Category');
require('./models/Examiners');
require('./models/Objective');
require('./models/Subjective');
require('./models/testDraft');
require('./models/Tests');
require('./models/Users');
require('./config/passport');

// Use either local mongodb or server mongodb
// var mongoIP = 'mongodb://10.10.233.175:27017/screeningtesttester'; // Server MongoDB
var mongoIP = 'mongodb://localhost:27017/screeningtesttester'; // Local MongoDB
// var mongoIP = (process.env.OPENSHIFT_MONGODB_DB_URL + 'nodejs' || 'mongodb://localhost:27017/screeningtesttester'); // OpenShift MongoDB
mongoose.connect(mongoIP);
// -------------- End --------------

/* Check the MongoDB database connection using Mongoose */
// ----------------------checking start------------------------------
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
	console.log('Successfully connected to MongoDB at ' + mongoIP);
});
// *----------------------checking end--------------------------------

// Include all route files
var admin = require('./routes/admin');
var candidate = require('./routes/candidate');
var categories = require('./routes/categories');
var common = require('./routes/common');
var examiner = require('./routes/examiner');
var questions = require('./routes/questions');
var testDraft = require('./routes/testDraft');
var tests = require('./routes/tests');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

app.use(function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
	next();
});

// Register route files to Express
app.use('/admin', admin);
app.use('/candidate', candidate);
app.use('/categories', categories);
app.use('/common', common);
app.use('/examiner', examiner);
app.use('/questions', questions);
app.use('/testDraft', testDraft);
app.use('/tests', tests);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function (err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});

console.log("Server started!");
module.exports = app;